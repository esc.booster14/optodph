<?php

namespace App;

use App\Observers\OperatorObserver;
use Illuminate\Database\Eloquent\Model;

class Operator extends Model
{
    public $timestamps = false;

    protected $table = 'operators';
    protected $fillable = [
        'id',
        'user_id',
        'type_id',
        'name',
        'company',
        'company_logo',
        'email',
        'address'
    ];

    protected $hidden = ['token'];

    # relationships

    public function user(){
    	return $this->belongsTo('App\User')->withDefault();
    }

    # static

    static function findCompany($value){
        return Operator::whereCompany($value)->first() ?: null;
    }

    static function findEmail($value){
        return Operator::whereEmail($value)->first() ?: null;
    }

    static function check($token){
        return Operator::whereToken($token)->first() ?: abort(404); 
    }

    # boot

    public static function boot() {
        parent::boot();

        Operator::observe(OperatorObserver::class);
    }
}
