<?php

namespace App;

use App\UserContact;
use Illuminate\Database\Eloquent\Model;

class UserContact extends Model
{
    public $timestamps = false;
    protected $table = 'user_contacts';
    
    protected $fillable = [
        'id',
        'user_id',
        'email',
        'phone_no',
        'telephone_no',
        'instagram',
        'twitter',
        'facebook'
    ];

    # relationships

    public function user()
    {
    	return $this->belongsTo('App\User');
    }

    # appends

    protected $appends = [
        'formatted_phone'
    ];
    
    public function getFormattedPhoneAttribute()
    {
        return !empty($this->phone_no) ? '63'. substr($this->phone_no, 1) : '';
    }

    # static function

    static function phoneExist($value)
    {
        return UserContact::wherePhoneNo($value)->count()!=0;
    }

    static function emailExist($value)
    {
        return UserContact::whereEmail($value)->count()!=0;
    }
}
