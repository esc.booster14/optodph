<?php

namespace App\Rules;

use Illuminate\Contracts\Validation\Rule;

class Username implements Rule
{
    /**
     * Create a new rule instance.
     *
     * @return void
     */
    public function __construct()
    {
        //
    }

    /**
     * Determine if the validation rule passes.
     *
     * @param  string  $attribute
     * @param  mixed  $value
     * @return bool
     */
    public function passes($attribute, $value)
    {
        return preg_match("/^((?!09).)(?![_.])(?=.*[a-zA-Z])([a-zA-Z0-9._]+)(?<![_.])$/", $value);
    }

    /**
     * Get the validation error message.
     *
     * @return string
     */
    public function message()
    {
        return "The username field is invalid. <br> > must not starts with '09' <br> > must not contain special characters. <br> > leading underscore or period is not allowed. <br> > underscore or period at last is not allowed. ";
    }
}
