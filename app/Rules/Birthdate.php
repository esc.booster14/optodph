<?php

namespace App\Rules;

use Carbon\Carbon;
use Illuminate\Contracts\Validation\Rule;

class Birthdate implements Rule
{
    /**
     * Create a new rule instance.
     *
     * @return void
     */

    public $age;

    public function __construct($age)
    {
        $this->age = $age; 
    }

    /**
     * Determine if the validation rule passes.
     *
     * @param  string  $attribute
     * @param  mixed  $value
     * @return bool
     */
    public function passes($attribute, $value)
    {
        $date = new Carbon($value);
        return $date->diff(now(), false)->y >= $this->age;
    }

    /**
     * Get the validation error message.
     *
     * @return string
     */
    public function message()
    {
        return 'Your age is not qualified to be a driver.';
    }
}
