<?php

namespace App\Helpers;

use ParagonIE\ConstantTime\Base64UrlSafe;
use Ramsey\Uuid\Uuid;

class RandomHelper{

	static function token($column = ''){
		return Uuid::uuid4() .'-'. Base64UrlSafe::encode(random_bytes(9)) .$column;
	}
}