<?php

namespace App\Helpers;

use Carbon\Carbon;
use Faker\Provider\DateTime;

class DateHelper{

	static function compare($date1, $date2){
		$DATE1 = new DateTime($date1);
		$DATE2 = new DateTime($date2);

		return $DATE1 > $DATE2;
	}

	static function format($date, $format = 'g:i A | d F Y'){
        $date = Carbon::parse($date);
        $date->setTimezone('Asia/Taipei');
        return $date->format($format);
	}

	static function subtractTo($date1, $date2, $unit){
		$DATE1 = new Carbon($date1);
        $DATE2 = new Carbon($date2);

        if($unit=="seconds")
        	return $DATE2->diffInSeconds($DATE1, false);
        if($unit=="minutes")
        	return $DATE2->diffInMinutes($DATE1, false);
		if($unit=="hours")
			return $DATE2->diffInHours($DATE1, false);
		if($unit=="days")
			return $DATE2->diffInDays($DATE1, false);
		if($unit=="weeks")
			return $DATE2->diffInWeeks($DATE1, false);
		if($unit=="months")
			return $DATE2->diffInMonths($DATE1, false);
		if($unit=="years")
			return $DATE2->diffInYears($DATE1, false);
	}

	static function timeElapsed($datetime, $full = false) {
	    $now = new DateTime;
	    $ago = new DateTime($datetime);
	    $diff = $now->diff($ago);

	    $diff->w = floor($diff->d / 7);
	    $diff->d -= $diff->w * 7;

	    $string = array(
	        'y' => 'year',
	        'm' => 'month',
	        'w' => 'week',
	        'd' => 'day',
	        'h' => 'hour',
	        'i' => 'minute',
	        's' => 'second',
	    );
	    foreach ($string as $k => &$v) {
	        if ($diff->$k) {
	            $v = $diff->$k . ' ' . $v . ($diff->$k > 1 ? 's' : '');
	        } else {
	            unset($string[$k]);
	        }
	    }

	    if (!$full) $string = array_slice($string, 0, 1);
	    return $string ? implode(', ', $string) . ' ago' : 'just now';
	}
}