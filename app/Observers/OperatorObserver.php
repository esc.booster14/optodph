<?php

namespace App\Observers;

use App\Helpers\RandomHelper;
use App\Operator;

class OperatorObserver
{
    /**
     * Handle the operator "created" event.
     *
     * @param  \App\Operator  $operator
     * @return void
     */
    public function creating(Operator $operator)
    {
        $operator->token = RandomHelper::token('operator');
    }

    public function created(Operator $operator)
    {
        //
    }

    /**
     * Handle the operator "updated" event.
     *
     * @param  \App\Operator  $operator
     * @return void
     */
    public function updated(Operator $operator)
    {
        //
    }

    /**
     * Handle the operator "deleted" event.
     *
     * @param  \App\Operator  $operator
     * @return void
     */
    public function deleted(Operator $operator)
    {
        //
    }

    /**
     * Handle the operator "restored" event.
     *
     * @param  \App\Operator  $operator
     * @return void
     */
    public function restored(Operator $operator)
    {
        //
    }

    /**
     * Handle the operator "force deleted" event.
     *
     * @param  \App\Operator  $operator
     * @return void
     */
    public function forceDeleted(Operator $operator)
    {
        //
    }
}
