<?php

namespace App\Http\Controllers;

use App\Helpers\DateHelper;
use App\Http\Requests\RegisterRequest;
use App\Notifications\MailNotification;
use App\User;
use Illuminate\Http\Request;

class RegisterController extends Controller
{
	public function storeDriver(RegisterRequest $data)
	{
		$data->validateDriver();

		$user = $this->createDriver($data->all());

		session()->forget('created_at');

		$this->generateCode($user->token);

		return redirect()->route('register.verification', $user->token);
	}

	public function createDriver(array $data)
	{
        $user = User::create([
            'name' => $data['first_name'] .' '. $data['family_name'],
            'username' => $data['phone'],
            'password' => bcrypt($data['password']),
            'type_id' => 3
        ]);

        $user->driver()->create([
        	'user_id' => $user->id,
        	'first_name' => $data['first_name'],
        	'family_name' => $data['family_name'],
        	'birthdate' => $data['birthdate'],
        	'gender' => $data['gender']
        ]);

        $user->contact()->create([
            'user_id' => $user->id,
            'phone_no' => $data['phone']
        ]);

        return $user;
	}

    public function storeOperator(RegisterRequest $data)
    {
    	$data->validateOperator();

    	$user = $this->createOperator($data->all());

    	$this->generateCode($user->token);

    	return redirect()->route('register.verification', session('token'))->with('success', 'The verification code was successfully sent.');
    }

	public function generateCode($token){
		$user = User::check($token);

		$sec = DateHelper::subtractTo(now(), session('created_at'), 'seconds');

		if(session()->has('created_at') && $sec < 150)
			return 'You have remaining '.(150-$sec).' seconds to resend again.';

		$this->updateSession($user->token);
		
		if($user->type_id==2)
			$user->notify(new MailNotification());
		if($user->type_id==3)
			$this->sendSms($user);

		return 'The verification code was successfully sent.';
	}

	protected function sendSms(User $user){
		$basic  = new \Nexmo\Client\Credentials\Basic('24e283dc', 'owQXacY2IuMhrQkI');
		$client = new \Nexmo\Client($basic);

		$message = $client->message()->send([
		    'to' => $user->contact->formatted_phone,
		    'from' => 'Optodph',
		    'text' => "Optodph verification code is \"" .session('code'). "\".\nThis is valid in 5 minutes.\n\n\n\n\n|"
		]);
	}

	public function verification($token){
		$this->checkToken($token);

		$user = User::whereToken($token)->first();

		return view('auth.registers.verification', compact('user'));
	}

	public function verify(RegisterRequest $data){
    	$data->merge([
    		'min' => DateHelper::subtractTo(now(), session('created_at'), 'minutes'),
    		'token' => session('token')
    	]);

		$data->validateVerify();
 		
		return $this->redirectHome($this->updateStatus());
	}
	
	public function redirect($token, $code){
		$data = new RegisterRequest([
			'min' => DateHelper::subtractTo(now(), session('created_at'), 'minutes'),
			'token' => $token,
			'code' => $code
		]);

		$data->validateRedirect();

		return $this->redirectHome($this->updateStatus());
	}

	# helpers

	protected function updateSession($token)
	{
		session(['code' => rand(1000,9999), 'created_at' => now(), 'token' => $token ]);
	}

	protected function redirectHome($id){
		session()->forget(['code', 'token']);

		auth()->loginUsingId($id);
		return redirect('home');
	}

	protected function checkToken($token){
		return session('token')==$token ?: abort(404);
	}

	protected function updateStatus(){
		$user = User::whereToken(session('token'))->first();

		$user->update(['status' => 2]);

		return $user->id;
	}

	protected function createOperator(array $data){
        $user = User::create([
            'name' => $data['operator'],
            'username' => $data['email'],
            'password' => bcrypt($data['password']),
            'type_id' => 2
        ]);

        $user->operator()->create([
            'user_id' => $user->id,
            'type_id' => 2,
            'name' => $data['operator'],
            'company' => $data['company'],
            'email' => $data['email'],
            'address' => $data['address']
        ]);

        $user->contact()->create([
            'user_id' => $user->id,
            'email' => $data['email']
        ]);

        return $user;
    }
}
