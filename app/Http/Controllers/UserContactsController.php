<?php

namespace App\Http\Controllers;

use App\UserContacts;
use Illuminate\Http\Request;

class UserContactsController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\UserContacts  $userContacts
     * @return \Illuminate\Http\Response
     */
    public function show(UserContacts $userContacts)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\UserContacts  $userContacts
     * @return \Illuminate\Http\Response
     */
    public function edit(UserContacts $userContacts)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\UserContacts  $userContacts
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, UserContacts $userContacts)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\UserContacts  $userContacts
     * @return \Illuminate\Http\Response
     */
    public function destroy(UserContacts $userContacts)
    {
        //
    }
}
