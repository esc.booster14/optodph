<?php

namespace App\Http\Requests;

use App\Operator;
use App\Rules\Birthdate;
use App\User;
use App\UserContact;
use Illuminate\Foundation\Http\FormRequest;

class RegisterRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            //
        ];
    }

    public function validateOperator(){
        $this->validate([
            'operator_type' => [
                'required','numeric',
                function($attribute, $value, $fail){
                    if($value<1 || $value>3)
                        $fail('The operator type is invalid.');
                }
            ],
            'company' => [
                'required','max:50',
                function($attribute, $value, $fail){
                    if(Operator::findCompany($value)){
                        $fail('The company has already taken.');
                    }
                }
            ],
            'operator' => "required|min:2|max:100|regex:/^(?![-])(?!.*[-]{2})[-a-zA-Z' ñ](.*[^-]$)*$/",
            'email' => [
                'required', 'min:3', 'max:64','email','confirmed',
                function($attribute, $value, $fail){
                    !Operator::findEmail($value) ?: $fail('The email has already taken.');
                }
            ],
            'address' => 'required|min:3|max:50',
            'password' => 'required|confirmed',
            'captcha' => 'required|captcha'
        ],['captcha' => 'The captcha field is incorrect.']);
    }

    public function validateDriver(){
        $this->validate([
            'first_name' => "required|min:2|max:50|regex:/^(?![-])(?!.*[-]{2})[-a-zA-Z' ñ](.*[^-]$)*$/",
            'family_name' => "required|min:2|max:50|regex:/^(?![-])(?!.*[-]{2})[-a-zA-Z' ñ](.*[^-]$)*$/",
            'birthdate' => ['required','date', new Birthdate(18)],
            'gender' => [
                'required','numeric',
                function($attribute, $value, $fail){
                    $value>=1 && $value<=2 ?: $fail('The gender field is invalid.');
                }
            ],
            'driver_type' => [
                'required','numeric',
                function($attribute, $value, $fail){
                    $value>=1 && $value<=3 ?: $fail('The driver type field is invalid.');
                }
            ],
            'phone' => [
                'required','confirmed','numeric','digits:11','regex:/(09)[0-9]{9}/',
                function($attribute, $value, $fail){
                    !UserContact::phoneExist($value) ?: $fail('The contact number has already taken.');
                }
            ],
            'password' => 'required|min:5|max:18|confirmed',
            'captcha' => 'required|captcha'
        ],[
            'phone.regex' => 'The phone number is invalid format.',
            '*.numeric' => 'The :attribute is invalid.',
            'captcha' => 'The captcha field is incorrect.'
        ]);
    }

    public function validateVerify(){
        $this->validate([
            'code' => [
                'required','numeric','digits:4',
                function($attribute, $value, $fail){
                    $value==session('code') ?: $fail('The code is incorrect.');
                }
            ],
            'min' => [
                'required','numeric',
                function($attribute, $value, $fail){
                    $value<=5 ?: $fail('The verification code was already expired.');
                }
            ],
            'token' => [
                'required',
                function($attribute, $value, $fail){
                    User::check($value);
                }
            ]
        ]);
    }

    public function validateRedirect(){
        $this->validate([
            'code' => [
                'required','numeric','digits:4',
                function($attribute, $value, $fail){
                    $value==session('code') ?: abort(404);
                }
            ],
            'min' => [
                'required','numeric',
                function($attribute, $value, $fail){
                    $value<=5 ?: abort(404);
                }
            ],
            'token' => [
                'required',
                function($attribute, $value, $fail){
                    User::check($value);
                }
            ]
        ]);
    }
}
