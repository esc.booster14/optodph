<?php

namespace App;

use App\Driver;
use App\Observers\DriverObserver;
use Illuminate\Database\Eloquent\Model;

class Driver extends Model
{
    public $timestamps = false;

    protected $table = 'drivers';
    protected $fillable = [
        'id',
        'token',
        'user_id',
        'first_name',
        'middle_name',
        'family_name',
        'suffix',
        'birthdate',
        'gender',
        'work_experience',
        'school_primary',
        'school_secondary',
        'school_tertiary',
        'height',
        'weight',
        'religion',
        'marital_status',
        'nationality',
        'address',
        'barangay',
        'city'
    ];

    # relationships

    public function user()
    {
        return $this->belongsTo('App\User');
    }

    # boot

    public static function boot() {
        parent::boot();

        Driver::observe(DriverObserver::class);
    }

}














