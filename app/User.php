<?php

namespace App;

use App\Observers\UserObserver;
use App\User;
use Illuminate\Contracts\Auth\MustVerifyEmail;
use Illuminate\Foundation\Auth\User as Authenticatable;
use Illuminate\Notifications\Notifiable;

class User extends Authenticatable
{
    use Notifiable;

    protected $fillable = [
        'name',
        'username',
        'type_id',
        'status',
        'active_status',
        'password',
        'image',
        'created_at',
        'updated_at'
    ];

    protected $hidden = [
        'password',
        'token',
        'remember_token',
    ];

    protected $guarded = [
        'password', 
        'token', 
        'type_id', 
        'status', 
        'active_status'
    ];

    # relationships

    public function operator()
    {
        return $this->hasOne('App\Operator')->withDefault();
    }

    public function driver()
    {
        return $this->hasOne('App\Driver')->withDefault();
    }

    public function contact()
    {
        return $this->hasOne('App\UserContact')->withDefault();
    }

    # appends

    protected $appends = ['type', 'email', 'phone_no'];
    
    public function getTypeAttribute(){
        return UserType::find($this->type_id)->name;
    }

    public function getEmailAttribute(){
        return $this->contact->email;
    }

    public function getPhoneNoAttribute(){
        return $this->contact->phone_no;
    }

    # helper
    public function isAdmin()
    {
        return $this->type_id==1;
    }

    public function isOperator()
    {
        return $this->type_id==2;
    }

    public function isDriver()
    {
        return $this->type_id==3;
    }

    # static

    static function check($token){
        return User::whereToken($token)->first() ?: abort(404); 
    }

    # boot

    public static function boot() {
        parent::boot();

        User::observe(UserObserver::class);
    }
}
