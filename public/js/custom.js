function alert_modal($type, $title, $msg){
  swal.fire({
      title: $title,
      text: $msg,
      type: $type
    })
}

function modal_preview(image){ 
  swal.fire({ 
    customClass: {
      popup: 'container-dim',
      image: 'img-fluid'
    }, 
    imageUrl: image, 
    imageAlt: 'Custom image',
    html: "<a href='"+ image +"' target='_blank'><i class='fa fa-external-link'></i> open file</a>",
    animation: false,
    heightAuto: true,
    showCloseButton: true,
    showConfirmButton: false
  })
}

function alert_confirm(form, msg = 'proceed') { 
  $(form).submit(function(event) {

    $flag = $(this).attr('data-flag');

    if ($flag == 0) {
      event.preventDefault();

      swal.fire({
        title: 'Are you sure?',
        text: 'You want to '+ msg +'? This process cannot be undone',
        type: 'warning',
        showCancelButton: true,
        reverseButtons: true,
        confirmButtonColor: '#3085d6',
        cancelButtonClass: 'bg-light-grey',
        confirmButtonText: 'Continue',
        cancelButtonText: 'Go back'
      }).then((result) => {
        if (result.value) {
            $(form).attr('data-flag', 1);
            $(form).submit(); 
        }
      })
    }

    return true;
  });
}

function link_confirm(link, msg = 'proceed')
{   
    swal.fire({
      title: 'Are you sure?',
      text: 'You want to ' + msg + '? This process cannot be undone',
      type: 'warning',
      showCancelButton: true,
      reverseButtons: true,
      confirmButtonColor: '#3085d6',
      cancelButtonClass: 'bg-light-grey',
      confirmButtonText: 'Continue',
      cancelButtonText: 'Go back'
    }).then((result) => {
      if (result.value) { 
        location.href = link;
      }
    })
}

function preload(){
  $('#preload').removeClass('d-none');
}

function preloadExit(){
  $('#preload').addClass('d-none');
}

// see more link 
$('.see-more').click(function(event) {
  var content = $(this).attr('data-content');
  var target = $(this).attr('data-target');
  
  var targetValue = $(target).val();
  if(targetValue=='')
    targetValue = $(target).html();

  $('.see-more').attr('data-content', targetValue);
  $(target).html(content.trim());

  if ($(this).html() == "see more")
    $(this).html('minimize');
  else
    $(this).html('see more');

  $(target).click(function(event) {
    $(this).height(0);
    $(this).height(this.scrollHeight);
  });

  $(target).click();
});