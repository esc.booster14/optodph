<footer class="footer">
    <div class="container-fluid">
        <ul class="nav">
            <!-- <li class="nav-item">
                <a href="https://creative-tim.com" target="blank" class="nav-link">
                    {{ _('Creative Tim') }}
                </a>
            </li>
            <li class="nav-item">
                <a href="https://updivision.com" target="blank" class="nav-link">
                    {{ _('Updivision') }}
                </a>
            </li> -->
            <li class="nav-item">
                <a href="#" class="nav-link">
                    {{ _('About Us') }}
                </a>
            </li>
            <li class="nav-item">
                <a href="#" class="nav-link">
                    {{ _('FAQ') }}
                </a>
            </li>
        </ul>
        <div class="copyright">
            &copy; 2018 - 2019 {{ _('made with') }} {{ _('by') }}
            <a href="https://creative-tim.com" target="_blank">Team Optodph</a> &amp;
            {{ _('for a better web') }}.
        </div>
    </div>
</footer>
