<div class="sidebar">
    <div class="sidebar-wrapper">
        <div class="logo" align="center">
            <!-- <a href="#" class="simple-text logo-mini">{{ _('BD') }}</a> -->
            <a href="{{ route('home') }}" class="simple-text logo-normal"><b class="h4 text-white no">OPTODPH</b></a>
        </div>
        <ul class="nav">
            <li @if ($pageSlug == 'dashboard') class="active " @endif>
                <a href="{{ route('home') }}">
                    <i class="tim-icons icon-chart-pie-36"></i>
                    <p>{{ _('Dashboard') }}</p>
                </a>
            </li>

            @if(auth()->user()->type==1)
                <li @if ($pageSlug == 'drivers') class="active " @endif>
                    <a href="{{ route('admin.users') }}">
                        <i class="tim-icons icon-single-02"></i>
                        <p>{{ _('Users') }}</p>
                    </a>
                </li>
                <li @if ($pageSlug == 'accounts') class="active " @endif>
                    <a href="{{ route('admin.transactions') }}">
                        <i class="tim-icons icon-credit-card"></i>
                        <p>{{ _('Transactions') }}</p>
                    </a>
                </li>
                <li @if ($pageSlug == 'accounts') class="active " @endif>
                    <a href="{{ route('admin.authenticate_user') }}">
                        <i class="tim-icons icon-credit-card"></i>
                        <p>{{ _('Authentication') }}</p>
                    </a>
                </li>
            @else
                <li @if ($pageSlug == 'home') class="active " @endif>
                    <a href="{{ route('profile.index', auth()->user()->username) }}">
                        <i class="tim-icons icon-molecule-40"></i>
                        <p>{{ _('News Feed') }}</p>
                    </a>
                </li> 
                <li @if ($pageSlug == 'jobs') class="active " @endif>
                    <a href="{{ route('jobs.index') }}">
                        <i class="tim-icons icon-notes"></i>
                        <p>{{ _('Jobs') }}</p>
                    </a>
                </li>

                @if(auth()->user()->type==2)
                    <li @if ($pageSlug == 'rental') class="active " @endif>
                        <a href="{{ route('rentals.index') }}">
                            <i class="tim-icons icon-badge"></i>
                            <p>{{ _('Rental') }}</p>
                        </a>
                    </li>
                    <li @if ($pageSlug == 'drivers') class="active " @endif>
                        <a href="{{ route('drivers.index') }}">
                            <i class="tim-icons icon-single-02"></i>
                            <p>{{ _('Drivers') }}</p>
                        </a>
                    </li>
                    <li @if ($pageSlug == 'vehicles') class="active " @endif>
                        <a href="{{ route('vehicles') }}">
                            <i class="tim-icons icon-bus-front-12"></i>
                            <p>{{ _('Vehicles') }}</p>
                        </a>
                    </li>
                    <li @if ($pageSlug == 'accounts') class="active " @endif>
                        <a href="{{ route('accounts.usage', auth()->user()->token) }}">
                            <i class="tim-icons icon-credit-card"></i>
                            <p>{{ _('Accounts') }}</p>
                        </a>
                    </li>
                @endif
            @endif
        </ul>
    </div>
</div>
