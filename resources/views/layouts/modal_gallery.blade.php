<div class="modal modal-info fade" id="div-mod-gallery" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
    <div class="modal-pos">
        <div class="modal-dialog">
            <div class="modal-content">
                <div class="modal-header padding-10">
                    <span class="h6" id="title-mod-gallery"></span>
                    <button type="button" class="close" data-dismiss="modal"><span aria-hidden="true"><i class="fa fa-close"></i></span><span class="sr-only">Close</span></button>
                </div>
                <div class="modal-body nopadding">
                    <div class="modal-body-2 padding-lr-10">
                        <div class="margin-auto">
                            <img id="img-mod-gallery" class="img-fluid">
                        </div>
                    </div>
                </div>
                <div class="modal-footer full-width padding-5">
                    <a class="btn btn-primary" id="show-previous-image"><i class="fa fa-backward"></i></a>
                    <!-- <a class="btn btn-primary" id="link-mod-gallery" target="_blank"><i class="fa fa-external-link"></i> </a> -->
                    <a class="btn btn-primary" id="show-next-image"><i class="fa fa-forward"></i></a>
                </div>
            </div>
        </div>
    </div>
</div>