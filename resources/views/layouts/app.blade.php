<!DOCTYPE html>
<html lang="{{ str_replace('_', '-', app()->getLocale()) }}">
    <head>
        <meta charset="UTF-8">
        <meta http-equiv="X-UA-Compatible" content="ie=edge">
        <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">

        <meta name="csrf-token" content="{{ csrf_token() }}">
 
        <!-- Favicon -->
        <link rel="apple-touch-icon" sizes="76x76" href="/img/apple-icon.png">
        <link rel="stylesheet" href="/css/preloader.css">
        <link rel="icon" type="image/png" href="/img/icon.png">
        <!-- Fonts -->
        <link href="/css/googleapis.css" rel="stylesheet" />  
        <!-- Icons -->
        <link href="/css/nucleo-icons.css" rel="stylesheet" />
        <link rel="stylesheet" href="/css/app.css">
        <!-- CSS -->
        <link href="/css/black-dashboard.css?v=1.0.0" rel="stylesheet" />
        <link href="/css/theme.css" rel="stylesheet" />

        <link rel="stylesheet" href="/css/custom.css">
        
        <style>
            .badge-top { position: absolute; top: 0; right: 0px; padding: 0.5px; background: -webkit-linear-gradient(top, #FF6969 0%, #ff0000 100%); box-shadow: 0 1px 2px rgba(0, 0, 0, .5), 0 1px 4px rgba(0, 0, 0, .4), 0 0 1px rgba(0, 0, 0, .7) inset, 0 10px 0px rgba(255, 255, 255, .11) inset; -webkit-background-clip: padding-box; font: bold 16px/20px "Helvetica Neue", sans-serif; color: white; text-decoration: none; text-shadow: 0 -1px 0 rgba(0, 0, 0, .6); font-size:11px;}
        </style>

        @yield('style')
    </head>
    <body class="{{ $class ?? '' }}">        
        <div id="app">
            <div id="preload" class="loaderBox">
                <div class="loadAnim">
                    <div class="loadeAnim1"></div>
                    <div class="loadeAnim2"></div>
                    <div class="loadeAnim3"></div>
                </div>
            </div>

            @auth()
                <div class="wrapper">
                    @include('layouts.navbars.sidebar')
                    <div class="main-panel">
                        @include('layouts.navbars.navbar')
                        <div class="content">
                            @yield('content')
                        </div>
                        @include('layouts.footer')
                    </div>
                </div>
                <form id="logout-form" action="{{ route('logout') }}" method="POST" style="display: none;">
                    @csrf
                </form>
                
                <div class="fixed-plugin">
                <div class="dropdown show-dropdown">
                    <a href="#" data-toggle="dropdown">
                        <i class="fa fa-cog fa-2x"> </i>
                    </a>
                    <ul class="dropdown-menu">
                        <li class="header-title"> Sidebar Background</li>
                        <li class="adjustments-line">
                            <a href="javascript:void(0)" class="switch-trigger background-color">
                                <div class="badge-colors text-center">
                                    <span class="badge filter badge-primary active" data-color="primary"></span>
                                    <span class="badge filter badge-info" data-color="blue"></span>
                                    <span class="badge filter badge-success" data-color="green"></span>
                                </div>
                                <div class="clearfix"></div>
                            </a>
                        </li>
                        <li class="adjustments-line text-center color-change">
                            <span class="color-label">LIGHT MODE</span>
                            <span class="badge light-badge mr-2"></span>
                            <span class="badge dark-badge ml-2"></span>
                            <span class="color-label">DARK MODE</span>
                        </li>
                        <li class="button-container">
                            <a href="https://www.creative-tim.com/product/black-dashboard-laravel" target="_blank" class="btn btn-primary btn-block btn-round">Download Now</a>
                            <a href="https://demos.creative-tim.com/black-dashboard/docs/1.0/getting-started/introduction.html" target="_blank" class="btn btn-default btn-block btn-round">
                                Documentation
                            </a>
                        </li>
                        <li class="header-title">Thank you for 95 shares!</li>
                        <li class="button-container text-center">
                            <button id="twitter" class="btn btn-round btn-info"><i class="fab fa-twitter"></i> &middot; 45</button>
                            <button id="facebook" class="btn btn-round btn-info"><i class="fab fa-facebook-f"></i> &middot; 50</button>
                            <br>
                            <br>
                            <a class="github-button" href="https://github.com/creativetimofficial/black-dashboard-laravel" data-icon="octicon-star" data-size="large" data-show-count="true" aria-label="Star ntkme/github-buttons on GitHub">Star</a>
                        </li>
                    </ul>
                </div>
            </div>
            @else
            @include('layouts.navbars.navbar')
                <div class="wrapper wrapper-full-page">
                    <div class="full-page {{ $contentClass ?? '' }}">
                        <div class="content">
                            <div class="container">
                                @yield('content')
                            </div>
                        </div>
                        @include('layouts.footer')
                    </div>
                </div>
            @endauth
        </div>

        <script src="/js/app.js"></script> 
        <script src="/js/core/jquery.min.js"></script>
        <script src="/js/core/popper.min.js"></script>
        <script src="/js/core/bootstrap.min.js"></script>
        <script src="/js/plugins/perfect-scrollbar.jquery.min.js"></script>

        <script src="/js/black-dashboard.min.js?v=1.0.0"></script>
        <script src="/js/theme.js" defer></script> 

        <script src="/js/custom.js"></script>
        
        @yield('script')
        @stack('js')

        <script defer>
            $('#preload').addClass('d-none');
            $('input, textarea').prop('spellcheck', false);

            $().ready(function() {
                $sidebar = $('.sidebar');
                $navbar = $('.navbar');
                $main_panel = $('.main-panel');

                $full_page = $('.full-page');

                $sidebar_responsive = $('body > .navbar-collapse');
                sidebar_mini_active = true;
                white_color = false;

                window_width = $(window).width();

                fixed_plugin_open = $('.sidebar .sidebar-wrapper .nav li.active a p').html();

                $('.fixed-plugin a').click(function(event) {
                    if ($(this).hasClass('switch-trigger')) {
                        if (event.stopPropagation) {
                            event.stopPropagation();
                        } else if (window.event) {
                            window.event.cancelBubble = true;
                        }
                    }
                });

                $('.fixed-plugin .background-color span').click(function() {
                    $(this).siblings().removeClass('active');
                    $(this).addClass('active');

                    var new_color = $(this).data('color');

                    if ($sidebar.length != 0) {
                        $sidebar.attr('data', new_color);
                    }

                    if ($main_panel.length != 0) {
                        $main_panel.attr('data', new_color);
                    }

                    if ($full_page.length != 0) {
                        $full_page.attr('filter-color', new_color);
                    }

                    if ($sidebar_responsive.length != 0) {
                        $sidebar_responsive.attr('data', new_color);
                    }
                });

                $('.switch-sidebar-mini input').on("switchChange.bootstrapSwitch", function() {
                    var $btn = $(this);

                    if (sidebar_mini_active == true) {
                        $('body').removeClass('sidebar-mini');
                        sidebar_mini_active = false;
                        blackDashboard.showSidebarMessage('Sidebar mini deactivated...');
                    } else {
                        $('body').addClass('sidebar-mini');
                        sidebar_mini_active = true;
                        blackDashboard.showSidebarMessage('Sidebar mini activated...');
                    }

                    // we simulate the window Resize so the charts will get updated in realtime.
                    var simulateWindowResize = setInterval(function() {
                        window.dispatchEvent(new Event('resize'));
                    }, 180);

                    // we stop the simulation of Window Resize after the animations are completed
                    setTimeout(function() {
                        clearInterval(simulateWindowResize);
                    }, 1000);
                });

                $('.switch-change-color input').on("switchChange.bootstrapSwitch", function() {
                        var $btn = $(this);

                        if (white_color == true) {
                            $('body').addClass('change-background');
                            setTimeout(function() {
                                $('body').removeClass('change-background');
                                $('body').removeClass('white-content');
                            }, 900);
                            white_color = false;
                        } else {
                            $('body').addClass('change-background');
                            setTimeout(function() {
                                $('body').removeClass('change-background');
                                $('body').addClass('white-content');
                            }, 900);

                            white_color = true;
                        }
                });

                $('.light-badge').click(function() {
                    $('body').addClass('white-content');
                });

                $('.dark-badge').click(function() {
                    $('body').removeClass('white-content');
                });

                min();
            });
            
            @if(count($errors)>0) 
                toast.fire({
                    type: 'error',
                    customClass: 'toastx',
                    timer: 10000,
                    title: "{{ $errors->first() }}"
                }) 
            @endif

            @if(session()->has('error'))
                toast.fire({
                    type: 'error', 
                    customClass: 'toastx',
                    timer: 10000,
                    title: "{{ session()->get('error') }}"
                }) 
            @endif

            @if(session()->has('success'))
                toast.fire({
                    type: 'success',
                    customClass: 'toastx',
                    timer: 10000,
                    title: "{{ session()->get('success') }}"
                }) 
            @endif

            @if(session()->has('warning'))
                toast.fire({
                    type: 'warning',
                    customClass: 'toastx',
                    timer: 10000,
                    title: "{{ session()->get('warning') }}"
                }) 
            @endif

            @if(session()->has('info'))
                toast.fire({
                    type: 'info',
                    customClass: 'toastx',
                    timer: 10000,
                    title: "{{ session()->get('info') }}"
                }) 
            @endif

            @if(session()->has('question'))
                toast.fire({
                    type: 'question',
                    customClass: 'toastx',
                    timer: 10000,
                    title: "{{ session()->get('question') }}"
                }) 
            @endif
        </script>
        @stack('js')
    </body>
</html>
