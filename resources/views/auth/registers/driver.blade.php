@extends('layouts.app', ['class' => 'register-page', 'page' => _('register Page'), 'contentClass' => 'register-page'])

@section('style') 
<title>Register Driver</title>
<link rel="stylesheet" href="/css/style.css">
@endsection

@section('content')
	<div class="card padd-20">
		<div class="card-header">
			<div class="row">
				<div class="col-md-6">
					<div class="text-h2">Register as an Driver</div>
				</div>
				<div class="col-md-6" align="right">
					<a href="{{ route('register.operator') }}" class="btn3 btn3-primary">Switch to Operator</a>
				</div>
			</div>
		</div>
		<form action="{{ route('register.driver') }}" method="POST" id="form-driver" data-flag="0">
			@csrf
			<div class="card-body">
				<div class="row">
					<div class="col-md-4">
						<p for="">First Name</p>
						<input type="text" name="first_name" value="{{ old('first_name') }}" id="first_name" class="form-control{{ $errors->has('first_name') ? ' is-invalid' : '' }}" required>
						@if ($errors->has('first_name'))
						<span class="feedback-error" role="alert">
							{{ $errors->first('first_name') }}
						</span>
						@endif
					</div>
					<div class="col-md-4">
						<p for="">Family Name</p>
						<input type="text" name="family_name" value="{{ old('family_name') }}" id="family_name" class="form-control{{ $errors->has('family_name') ? ' is-invalid' : '' }}" required>
						@if ($errors->has('family_name'))
						<span class="feedback-error" role="alert">
							{{ $errors->first('family_name') }}
						</span>
						@endif
					</div>
					<div class="col-md-4">
						<p align="left">Birthdate</p>
						<input type="date" name="birthdate" value="{{ old('birthdate') }}" id="birthdate" class="form-control{{ $errors->has('birthdate') ? ' is-invalid' : '' }}" required>
						@if ($errors->has('birthdate'))
						<span class="feedback-error" role="alert">
							{{ $errors->first('birthdate') }}
						</span>
						@endif
					</div>

					<div class="col-md-4">
						<p>Gender</p>
						<input type="radio" name="gender" value="1" id="radio1" @if(old('gender')==1) checked @endif />
						<label for="radio1">Male</label>
						<input type="radio" name="gender" value="2" id="radio2" @if(old('gender')==2) checked @endif/>
						<label for="radio2">Female</label> 
						@if ($errors->has('gender'))
							<span class="feedback-error" role="alert" style="margin-top:-25px;">
								{{ $errors->first('gender') }}
							</span>
						@endif
					</div>
					<div class="col-md-4">
						<p for="">Phone</p>
						<input type="tel" pattern="[0-9]*" maxlength="11" class="form-control{{ $errors->has('phone') ? ' is-invalid' : '' }}"  value="09068812078" name="phone" id="phone" required placeholder="09XXXXXXXXX">
						@if ($errors->has('phone'))
						<span class="feedback-error" role="alert">
							{{ $errors->first('phone') }}
						</span>
						@endif
					</div>
					<div class="col-md-4">
						<p for="">Retype phone</p>
						<input type="tel" pattern="[0-9]*" maxlength="11" class="form-control{{ $errors->has('phone_confirmation') ? ' is-invalid' : '' }}"  value="{{ old('phone_confirmation') }}" name="phone_confirmation" id="phone_confirmation" required>
						@if ($errors->has('phone_confirmation'))
						<span class="feedback-error" role="alert">
							{{ $errors->first('phone_confirmation') }}
						</span>
						@endif
					</div>

					<div class="col-md-4">
						<p for="">Password</p>
						<input type="password" class="form-control{{ $errors->has('password') ? ' is-invalid' : '' }}"  value="{{ old('password') }}" name="password" id="password" required>
						@if ($errors->has('password'))
						<span class="feedback-error" role="alert">
							{{ $errors->first('password') }}
						</span>
						@endif
					</div>
					<div class="col-md-4">
						<p for="">Retype password</p>
						<input type="password" class="form-control{{ $errors->has('password_confirmation') ? ' is-invalid' : '' }}"  value="{{ old('password_confirmation') }}" name="password_confirmation" id="password_confirmation" required>
						@if ($errors->has('password_confirmation'))
						<span class="feedback-error" role="alert">
							{{ $errors->first('password_confirmation') }}
						</span>
						@endif
					</div>
					<div class="col-md-4">
						<p>Driver type</p> 
						<select name="driver_type" class="form-control selectpicker" data-style="btn btn-link" id="exampleFormControlSelect1" id="opty" required>
							<option value="">< Driver Type ></option>
							<option value="1" @if(old('driver_type')==1) selected @endif>Jeepney driver</option>
							<option value="2" @if(old('driver_type')==2) selected @endif>Taxi driver</option>
							<option value="3" @if(old('driver_type')==3) selected @endif>Bus driver</option>
						</select>
						@if ($errors->has('driver_type'))
						<span class="feedback-error" role="alert" style="margin-top:-25px;">
							{{ $errors->first('driver_type') }}
						</span>
						@endif
					</div>
				</div>

				<br>
				<div class="row">
					<div class="col-md-6">
						<div class="row no">
							<div class="col-md-6 no col-sm-12">
								<img src="{{ captcha_src('flat') }}" alt="captcha" class="captcha captcha-img" data-refresh-config="default">
								<p class="btn-refresh captcha-button-refresh text-h5 curs-pointer">Difficult to read? Click here to try again</p>
							</div>
							<div class="col-md-6 no col-sm-12">
								<p class="text-h5">Type the letter or number shown in the picture</p>
								<input id="captcha" class="captcha-field" name="captcha" required>
								@if ($errors->has('captcha'))
								<br>
								<span class="feedback-error" role="alert">
									{{ $errors->first('captcha') }}
								</span>
								@endif
							</div>
						</div>
					</div>
					<div class="col-md-6" align="right">
						<p class="form-check-p">
							<input class="form-check-input" name="agree" @if(old('agree')=='on') checked @endif id="agree" type="checkbox">
							<label class="curs-pointer" for="agree">
								<span class="form-check-sign"></span>
								{{ _('I agree to the') }}
								<a href="#">{{ _('terms and conditions') }}</a>.
							</label>
						</p>
					</div>
				</div>
			</div>

			<div class="card-footer">
				<button class="btn3 btn3-success">Register</button> 
			</div>
		</form>
	</div> 
@endsection

@section('script')
<script defer>
	$(document).ready(function() {
		$('#phone, #password, #phone_confirmation, #password_confirmation').on("cut copy paste", function(e) {
			e.preventDefault();
		});

		$('#form-driver').submit(function(event) { 
			$flag = $(this).attr('data-flag');

			if ($flag==0) { 
				event.preventDefault();

				if (!$('#agree').is(':checked')) { 
					swal.fire({
						title: 'Agree terms',
						text: 'You must checked the terms and conditions first.',
						type: 'info'
					})
					return false;
				}

				swal.fire({
					title: 'Are you sure?',
					text: 'You want to continue this registration? This process cannot be undone',
					type: 'warning',
					showCancelButton: true,
					reverseButtons: true,
					confirmButtonColor: '#3085d6',
					cancelButtonClass: 'bg-light-grey',
					confirmButtonText: 'Continue',
					cancelButtonText: 'Go back'
				}).then((result) => {
					if (result.value) {
						$('#preload').removeClass('d-none');
						$('#form-driver').attr('data-flag', 1);
						$('#form-driver').submit();
					}
				})
			}

			return true;
		});

		$(".btn-refresh").click(function() {
			$('img.captcha-img').prop('src', '/captcha/flat?' + Math.random());
		});
	});
</script>
@endsection