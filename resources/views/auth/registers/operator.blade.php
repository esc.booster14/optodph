@extends('layouts.app', ['class' => 'register-page', 'page' => _('register Page'), 'contentClass' => 'register-page'])

@section('style') 
<title>Register Operator</title>
@endsection

@section('content')
	<div class="card padd-20">
		<div class="card-header padding-bottom-10">
			<div class="row">
				<div class="col-md-6">
					<div class="text-h2">Register as an Operator</div>
				</div>
				<div class="col-md-6" align="right">
					<a href="{{ route('register.driver') }}" class="btn3 btn3-primary">Switch to driver</a>
				</div>
			</div>
		</div>
		<form action="{{ route('register.operator') }}" method="POST" id="register-operator" data-flag="0">
			@csrf
			<div class="card-body">
				<div class="row">
					<div class="col-md-4">
						<p for="company">Company</p>
						<input type="text" name="company" value="{{ old('company') }}" id="company" class="form-control{{ $errors->has('company') ? ' is-invalid' : '' }}" required>
						@if ($errors->has('company'))
						<span class="feedback-error" role="alert">
							{{ $errors->first('company') }}
						</span>
						@endif
					</div>
					<div class="col-md-4">
						<p for="operator">Operator</p>
						<input type="text" name="operator" value="{{ old('operator') }}" id="operator" class="form-control{{ $errors->has('operator') ? ' is-invalid' : '' }}" required>
						@if ($errors->has('operator'))
						<span class="feedback-error" role="alert">
							{{ $errors->first('operator') }}
						</span>
						@endif
					</div>
					<div class="col-md-4">
						<p for="opty">Operator type</p>
						<select name="operator_type" class="form-control selectpicker" data-style="btn btn-link" id="exampleFormControlSelect1" id="opty" required>
							<option value="">< Operator Type ></option>
							<option value="1" @if(old('operator_type')==1) selected @endif>Jeepney operator</option>
							<option value="2" @if(old('operator_type')==2) selected @endif>Taxi operator</option>
							<option value="3" @if(old('operator_type')==3) selected @endif>Bus operator</option>
						</select>
						@if ($errors->has('operator_type'))
							<span class="feedback-error" role="alert">
								{{ $errors->first('operator_type') }}
							</span>
						@endif
					</div>

					<div class="col-md-4">
						<p for="address">Address</p>
						<input name="address" class="form-control{{ $errors->has('address') ? ' is-invalid' : '' }}" type="address" id="address" value="{{ old('address') }}" required>
						@if ($errors->has('address'))
						<span class="feedback-error" role="alert">
							{{ $errors->first('address') }}
						</span>
						@endif
					</div>
					<div class="col-md-4">
						<p for="email">Email</p>
						<input name="email" class="form-control{{ $errors->has('email') ? ' is-invalid' : '' }}" type="email" id="email" value="{{ old('email') }}" required>
						@if ($errors->has('email'))
						<span class="feedback-error" role="alert">
							{{ $errors->first('email') }}
						</span>
						@endif
					</div>
					<div class="col-md-4">
						<p for="email_confirmation">Retype email</p>
						<input name="email_confirmation" class="form-control{{ $errors->has('email') ? ' is-invalid' : '' }}" type="email" id="email_confirmation" value="{{ old('email_confirmation') }}" required>
						@if ($errors->has('email_confirmation'))
						<span class="feedback-error" role="alert">
							{{ $errors->first('email_confirmation') }}
						</span>
						@endif
					</div>

					<div class="col-md-4">
						<p for="">Password</p>
						<input type="password" class="form-control{{ $errors->has('password') ? ' is-invalid' : '' }}"  value="{{ old('password') }}" name="password" id="password" required>
						@if ($errors->has('password'))
						<span class="feedback-error" role="alert">
							{{ $errors->first('password') }}
						</span>
						@endif
					</div>
					<div class="col-md-4">
						<p for="">Retype password</p>
						<input type="password" class="form-control{{ $errors->has('password_confirmation') ? ' is-invalid' : '' }}"  value="{{ old('password_confirmation') }}" name="password_confirmation" id="password_confirmation" required>
						@if ($errors->has('password_confirmation'))
						<span class="feedback-error" role="alert">
							{{ $errors->first('password_confirmation') }}
						</span>
						@endif
					</div>
				</div>
				
				<br>
				<div class="row">
					<div class="col-md-6">
						<div class="row no">
							<div class="col-md-6 no col-sm-12">
								<img src="{{ captcha_src('flat') }}" alt="captcha" class="captcha captcha-img" data-refresh-config="default">
								<p class="btn-refresh captcha-button-refresh text-h5 curs-pointer">Difficult to read? Click here to try again</p>
							</div>
							<div class="col-md-6 no col-sm-12">
								<p class="text-h5">Type the letter or number shown in the picture</p>
								<input id="captcha" class="captcha-field" name="captcha" required>
								@if ($errors->has('captcha'))
								<br>
								<span class="feedback-error" role="alert">
									{{ $errors->first('captcha') }}
								</span>
								@endif
							</div>
						</div>
					</div>
					<div class="col-md-6" align="right">
						<p class="form-check-p">
							<input class="form-check-input" name="agree" @if(old('agree')=='on') checked @endif id="agree" type="checkbox">
							<label class="curs-pointer" for="agree">
								<span class="form-check-sign"></span>
								{{ _('I agree to the') }}
								<a href="#">{{ _('terms and conditions') }}</a>.
							</label>
						</p>
					</div>
				</div>
			</div>

			<div class="card-footer">
				<button class="btn3 btn3-success">Register</button>
			</div>
		</form>
	</div>
@endsection

@section('script')
<script> 
	$('#email, #password, #email_confirmation, #password_confirmation').on("cut copy paste", function(e) {
		e.preventDefault();
	});

	$('#register-operator').submit(function(event) { 
		$flag = $(this).attr('data-flag');
		
		if ($flag==0) { 
			event.preventDefault();

			if (!$('#agree').is(':checked')) { 
				swal.fire({
					title: 'Agree terms',
					text: 'You must checked the terms and conditions first.',
					type: 'info'
				})
				return false;
			}

			swal.fire({
				title: 'Are you sure?',
				text: 'You want to continue this registration? This process cannot be undone',
				type: 'warning',
				showCancelButton: true,
				reverseButtons: true,
				confirmButtonColor: '#3085d6',
				cancelButtonClass: 'bg-light-grey',
				confirmButtonText: 'Continue',
				cancelButtonText: 'Go back'
			}).then((result) => {
				if (result.value) {
					preload();
					$('#register-operator').attr('data-flag', 1);
					$('#register-operator').submit();
				}
			})
		}

		return true;
	});

	$(".btn-refresh").click(function() {
		$('img.captcha-img').prop('src', '/captcha/flat?' + Math.random());
	}); 
</script>
@endsection