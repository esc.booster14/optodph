<form method="POST" action="#" class="form">
    @csrf
    <div class="xcd"></div>
    <div class="padding-20 ">
        <div class="row padding-10"><div class="col-12 text-center"><h4>Choose your plan</h4></div></div>
        <div class="row padding-20">
            <!-- Bronze -->
            <div class="col-md-4 padding-lr-5">
                <div class="bg-light-grey text-center" style="min-height:100px">
                    <div class="w3-card">
                        <div class="card-header padding-5 text-center w3-brown">
                            BRONZE
                        </div>
                        <div class="card-body nopadding">
                            <div class="w3-light-grey padding-15">
                                <h4 class="text-black" style="font-size:30px; font-family: arial; margin:0">₱ 0.00 <span style="font-size:13px">/ 15 Day</span></h4>
                                <p class="text-black" style="margin:0"><span style="font-size:13px">Try FREE Trial</span></p>
                            </div>
                            <div class="row nopadding"><div class="col-12 padding-5">1 Vehicle Type</div></div>
                            <div class="row nopadding"><div class="col-12 padding-5 w3-light-grey">1 Branch</div></div>
                            <div class="row nopadding"><div class="col-12 padding-5">Manage Drivers</div></div>
                            <div class="row nopadding"><div class="col-12 padding-5 w3-light-grey">Manage Vehicles <sub>(5x)</sub></div></div>
                            <div class="row nopadding"><div class="col-12 padding-5">Monitor Vehicle <span style="font-size:9px;">using MOBILE PHONE</span></div></div>
                            <div class="row nopadding"><div class="col-12 padding-5 w3-light-grey">Post Announcements <sub>(1x)</sub></div></div>
                            <div class="row nopadding"><div class="col-12 padding-5">Post Jobs <sub>(1x)</sub></div></div>
                            <div class="row nopadding"><div class="col-12 padding-5 w3-light-grey">Systematical Rental Tally</div></div>
                            <div class="padding-10 w3-light-grey">
                                <button class="btn btn-success bronze">SUBSCRIBE</button>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <!-- # Silver -->
            <div class="col-md-4 padding-lr-5">
                <div class="bg-light-grey text-center" style="min-height:100px">
                    <div class="w3-card">
                        <div class="card-header padding-5 text-center" style="background:#c0c0c0">
                            SILVER
                        </div>
                        <div class="card-body nopadding">
                            <div class="w3-light-grey padding-15">
                                <h4 class="text-black" style="font-size:30px; font-family: arial; margin:0">₱ 500.00 <span style="font-size:13px">/ month</span></h4>
                                <p class="text-black" style="margin:0"><span style="font-size:13px">Try MONTHLY Plan</span></p>
                            </div>
                            <div class="row nopadding"><div class="col-12 padding-5">1 Vehicle Type</div></div>
                            <div class="row nopadding"><div class="col-12 padding-5 w3-light-grey">1 Branches</div></div>
                            <div class="row nopadding"><div class="col-12 padding-5">Manage Drivers</div></div>
                            <div class="row nopadding"><div class="col-12 padding-5 w3-light-grey">&#9733; Manage Vehicles <sub>(30x)</sub></div></div>
                            <div class="row nopadding"><div class="col-12 padding-5">Monitor Vehicle <span style="font-size:9px;">using MOBILE PHONE</span></div></div>
                            <div class="row nopadding"><div class="col-12 padding-5 w3-light-grey">&#9733; Post Announcements</div></div>
                            <div class="row nopadding"><div class="col-12 padding-5">&#9733; Post Jobs</div></div>
                            <div class="row nopadding"><div class="col-12 padding-5 w3-light-grey">&#9733; <span style="font-size:11px">Auto-generated</span> Financial Statements</div></div>
                            <div class="row nopadding"><div class="col-12 padding-5">Systematical Rental Tally</div></div>
                            <div class="row nopadding"><div class="col-12 padding-5 w3-light-grey">&#9733; Manage Employee</div></div> 
                            <div class="padding-10 w3-light-grey border">
                                <button type="submit" class="btn btn-success silver">SUBSCRIBE</button>
                            </div>
                        </div>
                    </div>
                </div>
            </div>

            <!-- Gold -->
            <div class="col-md-4 padding-lr-5 gold">
                <div class="bg-light-grey text-center" style="min-height:100px">
                    <div class="w3-card">
                        <div class="card-header padding-5 text-center" style="background:#FFD700">
                            GOLD
                        </div>
                        <div class="card-body nopadding">
                            <div class="w3-light-grey padding-15">
                                <h4 class="text-black" style="font-size:30px; font-family: arial; margin:0">₱ 920.00 <span style="font-size:13px">/ month</span></h4>
                                <p class="text-black" style="margin:0"><span style="font-size:13px">Try YEARLY Plan</span></p>
                            </div>
                            <div class="row nopadding"><div class="col-12 padding-5">1 Vehicle Type</div></div>
                            <div class="row nopadding"><div class="col-12 padding-5 w3-light-grey">1 Branches</div></div>
                            <div class="row nopadding"><div class="col-12 padding-5">Manage Drivers</div></div>
                            <div class="row nopadding"><div class="col-12 padding-5 w3-light-grey">&#9733; Manage Vehicles <sub>(50x)</sub></div></div>
                            <div class="row nopadding"><div class="col-12 padding-5">Monitor Vehicle <span style="font-size:9px;">using MOBILE PHONE</span></div></div>
                            <div class="row nopadding"><div class="col-12 padding-5 w3-light-grey">Post Announcements</div></div>
                            <div class="row nopadding"><div class="col-12 padding-5">Post Jobs</div></div>
                            <div class="row nopadding"><div class="col-12 padding-5 w3-light-grey"> <span style="font-size:11px">Auto-generated</span> Financial Statements</div></div>
                            <div class="row nopadding"><div class="col-12 padding-5">Systematical Rental Tally</div></div>
                            <div class="row nopadding"><div class="col-12 padding-5 w3-light-grey"> Manage Employee</div></div> 
                            <div class="padding-10 w3-light-grey border">
                                <button class="btn btn-success gold">SUBSCRIBE</button>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</form>