@extends('layouts.app', ['page' => 'verification'])

@section('style')
	<title>Account Verification</title>
@endsection

@section('content')
	
	@if($user->isDriver())
		<form action="{{ route('register.verify') }}" method="POST"> 
			@csrf

			<h1>Verify Phone Number</h1>
			<p>A text message with a verification code has been sent to {{ $user->contact->formatted_phone }}</p>
			<a href="#send-code" class="send-code">Resend confirmation code</a>

			<p class="marg-t-15">Enter code here</p>
			<div class="row">
				<div class="col-1"><button class="btn3 btn3-primary">Verify</button></div>
				<div class="col-3 padd-n"><input type="text" name="code" class="form-control" placeholder="Enter 4 digit number"></div>
			</div>
		</form>
	@endif

	@if($user->isOperator())
		<h1>Verify Email Address</h1>
		<p>Please check your email inbox to confirm your account. Do not forget to check your spam folder as well.</p>
		<a href="#send-code" class="send-code">Resend confirmation email</a>
	@endif
	
	<p class="marg-t-10 bg-light-grey feedbackx">
		@if(count($errors)>0)
			{{ $errors->first() }}
		@endif
	</p>
@endsection

@section('script')
	<script defer>
		$('.send-code').click(function(event) { 
			preload();
			sendCode();
		});

		function sendCode(){
			$.ajax({
				method: 'POST',
				url: '{{ route('register.generate_code', $user->token) }}',
				data : {
					"_token": "{{ csrf_token() }}"
				},
				success: function (data) {
					$('.feedbackx').html(data).fadeIn('slow');

					$('#preload').addClass('d-none');

					if(data==null)
						location.href = "{{ route('home') }}";

					feed();
					preloadExit();
		        }
			});
		} 

		function feed(){
            setTimeout(function(){
                $('.feedbackx').fadeOut('slow');
            },5000);
		}
	</script>
@endsection