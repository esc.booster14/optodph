<html>
    <head>
        <title>Create an account</title> 
        <link rel="stylesheet" href="/css/preloader.css">
        <link rel="icon" type="image/png" href="/img/app/icon.png">
        <!-- Fonts -->
        <link href="/css/googleapis.css" rel="stylesheet" />  
        <!-- Icons -->
        <link href="/css/nucleo-icons.css" rel="stylesheet" />
        <link rel="stylesheet" href="/css/app.css">
        <!-- CSS -->
        <link href="/css/black-dashboard.css?v=1.0.0" rel="stylesheet" />
        <link href="/css/theme.css" rel="stylesheet" />
        <link rel="stylesheet" href="/css/style.css">
        <style>
            body{ background: #1e1e2f; }
            .cent{ margin:0 auto !important; }
            .register { padding: 3%; }
            .register-left { text-align: center; color: #fff; margin-top: 4%; }
            .register-left input, .login-butt { border: none; border-radius: 1.5rem; padding: 2%; width: 60%; background: #f8f9fa; font-weight: bold; color: #383d41; margin-top: 30%; margin-bottom: 3%; cursor: pointer; }
            .login-butt:hover{ text-decoration: none; }
            .register-right { background: #f8f9fa; border-top-left-radius: 10% 50%; border-bottom-left-radius: 10% 50%; }
            
            @-webkit-keyframes mover {
                0% { transform: translateY(0); }
                100% { transform: translateY(-20px); }
            }
            @keyframes mover {
                0% { transform: translateY(0); }
                100% { transform: translateY(-20px); }
            }
            .register-left p { font-weight: lighter; padding: 12%; margin-top: -9%; }
            .register .register-form { padding: 10%; margin-top: 10%; }
            .btnRegister { border: none; border-radius: 1.5rem; padding:5px; width:150px; background: #0062cc; color: #fff; font-weight: 600; cursor: pointer; }
            .register .nav-tabs { margin-top: 3%; margin-right: 0 !important; border: none; background: #0062cc; border-radius: 1.5rem; width: 28%; float: right; }
            .register .nav-tabs .nav-link { padding: 2%; height: 34px; font-weight: 600; color: #fff; border-top-right-radius: 1.5rem; border-bottom-right-radius: 1.5rem; text-align:center;}
            .register .nav-tabs .nav-link:hover { cursor:pointer; border: white; }
            .register .nav-tabs .nav-link.active { width: 100px; color: #0062cc; border: 2px solid #0062cc; border-top-left-radius: 1.5rem; border-bottom-left-radius: 1.5rem; }
            .register-heading { text-align: center; margin-top: 8%; margin-bottom: -15%; color: #495057; }
            
            input.radio{ display:none; }
            input.radio:empty ~ label { position: relative; float: left; line-height: 2.5em; text-indent: 2.8em;  cursor: pointer; -webkit-user-select: none; -moz-user-select: none; -ms-user-select: none; user-select: none; margin-right: 3px;}
            input.radio:empty ~ label:before { position: absolute; display: block; top: 0; bottom: 0; left: 0; content: ''; width: 2.5em; background: #D1D3D4; border-radius: 3px 0 0 3px;}
            input.radio:hover:not(:checked) ~ label:before { content: '\2714'; text-indent: .9em; color: #C2C2C2; }
            input.radio:hover:not(:checked) ~ label { color: #888; }
            input.radio:checked ~ label:before { content: '\2714'; text-indent: .9em; color: white; background-color: #4DCB6D; }
            input.radio:checked ~ label { color: #000; }
            input.radio:focus ~ label:before { box-shadow: 0 0 0 3px #999; }

            .outer { display: table; background: #1e1e2f; absolute; top: 0; left: 0; height: 100%; width: 100%; }
            .middle { display: table-cell; vertical-align: middle; }
            .card { background: #0074D9 !important; }
            input, select{ color: #000 !important; }
            h3, p { color:#000 !important; }
        </style>
        @yield('style')
    </head>
    <body>
        <div id="preload" class="loaderBox d-none">
            <div class="loadAnim">
                <div class="loadeAnim1"></div>
                <div class="loadeAnim2"></div>
                <div class="loadeAnim3"></div>
            </div>
        </div>
        <div class="outer">
            <div class="middle">
                <div class="container card register">
                    <div class="row">
                        <div class="col-md-3 col-sm-12 col-xs-12 order-md-1 order-sm-12 order-12 register-left" align="center">
                            <div style="height:10%"></div>
                            <h4 class="margin-top-60">Already have an account?</h4><br>
                            <a href="{{ route('login') }}" class="btn-block login-butt cent">Login</a><br/>
                        </div>
                        <div class="col-md-9 col-sm-12 col-xs-12 order-md-12 order-sm-1 order-1 register-right">
                            @yield('tab')

                            <div class="tab-content" id="myTabContent">
                                @yield('content')
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <script src="/js/app.js"></script> 
        <script src="/js/core/jquery.min.js"></script>
        <script src="/js/script.js" defer></script>
        @yield('script')
    </body>
</html>