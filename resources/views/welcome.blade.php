@extends('layouts.app')

@section('style')
<title>Optodph - Welcome Visitor</title> 

<link rel="stylesheet" href="/css/animate.css">
<link rel="stylesheet" href="/css/welcome.css"> 
<style>
    .text-black, .hover-text-black:hover { color: #000 !important }
    .bg-light-grey, .bg-hover-light-grey:hover, .bg-light-gray, .bg-hover-light-gray:hover { color: #000 !important; background-color: #f1f1f1 !important }
    .img-circle { border-radius: 50%; width: 150px; }
    ul { padding: 0 !important; }
    li { list-style-type: none; }
    .wel-p1 { overflow: hidden; padding: 6% 3%; }
    .wel-p1-p1 { padding: 2.2% 4% 0 4%; }
    .wel-p1-p2 img { max-width: 600px; }
    .wel-p1-p1 h1 { font-size: 4.5vmax; color: white; filter: drop-shadow(1px 1px black); }
    .wel-p1-p2 { margin: 1% 0; }
    .wel-p1-p1 p { font-size: 13px; color: white; }
    .wel-p3 { overflow: hidden; text-align: center; }
    .wel-p1-p2 img { margin-top: 1%; width: 90%; }
    .wel-p2 { padding: 3% 5%; }
    .wel-p2 h2 { font-family: Alegraya; }
    .wel-p2 input[type="submit"] { margin-top: 5px; }
    .wel-p3-title p { font-size: 20px; font-weight: bold; font-family: Arial; color: #E25D00; text-shadow: 0.1px 0.1px #4A0038; }
    .wel-p3 h1 { margin-bottom: 30px; font-size: 30px; }
    .wel-p3 h2 { font-size: 20px; margin-top: 15px; }
    .wel-p3 p { text-align: left; }
    .wel-p3 { padding: 5%; }
    .wel-p3 img { max-height: 170px; }
    .wel-p3 h2 { font-size: 20px; }
    .wel-p4 { padding: 10px; }
    .wel-p5 { padding: 50px 20px; }
    .wel-p5 { color: white; }
    .border-green { border: 1px solid green; }

    .mySlides { display: none; }
    .slideshow-container { max-width: 1000px; position: relative; margin: auto; }
    .prev, .next { cursor: pointer; position: absolute; top: 50%; width: auto; padding: 16px; margin-top: -22px; color: white; font-weight: bold; font-size: 18px; transition: 0.6s ease; border-radius: 0 3px 3px 0; user-select: none; }
    .next { right: 0; border-radius: 3px 0 0 3px; }
    .prev:hover, .next:hover { background-color: rgba(0, 0, 0, 0.8); }

    .text { color: #f2f2f2; font-size: 15px; padding: 8px 12px; position: absolute; bottom: 8px; width: 100%; text-align: center; }
    .numbertext { color: #f2f2f2; font-size: 12px; padding: 8px 12px; position: absolute; top: 0; }
    .dot { cursor: pointer; height: 15px; width: 15px; margin: 0 2px; background-color: #bbb; border-radius: 50%; display: inline-block; transition: background-color 0.6s ease; }
    .active, .dot:hover { background-color: #fefe; }
    @media only screen and (max-width:300px) { .prev, .next, .text { font-size: 11px } }
</style>
@endsection

@section('content')
<div class="wel" id="about">
    <span id="home"></span>
    <div class="wel-p1">
        <div class="row">
            <div class="col-lg-6 col-md-6 col-sm-12 wel-p1-p1">
                <h1>Online Platform For Transport Operators and Drivers</h1>
                <p>SYSTEMATIC WAY OF MANAGING DRIVERS AND VEHICLES</p>
                <a href="#features">
                    <div class="wel-butt-seefeatures btn btn-outline-light usr-link">SEE FEATURES</div>
                </a>
            </div>
            <div class="col-lg-6 col-md-6 wel-p1-p2 wel-p1-p2-img text-center">
                <img src="img/jeep.png" class="img-fluid">
            </div>
        </div>
    </div>  
    <br>

    @include('auth.registers._subscription')
    <br>
    
    <!-- # Features -->
    <span id="features"></span> 
    <div class="wel-p3">
        <h4>Features</h4> 
        <div class="row">
            <div class="col-lg-4 col-md-4 col-sm-12 wow fadeInLeft border-green" data-wow-duration="2s">
                <img src="img/m-drivers.png" alt="" class="img-fluid">
                <h2>Driver Management</h2>
                <ul>
                    <li>Manage Schedules</li>
                    <li>Manage Rental Payments</li>
                    <li>Add Feedbacks & Reviews</li>
                </ul>
            </div>
            <div class="col-lg-4 col-md-4 col-sm-12 wow fadeInLeft border-green" data-wow-duration="2s">
                <img src="img/m-vehicles.png" alt="" class="img-fluid">
                <h2>Vehicle Management</h2>
                <ul>
                    <li>Assign Driver</li>
                    <li>Monitor Vehicle using GPS</li>
                    <li>Create and Update History</li>
                </ul>
            </div>
            <div class="col-lg-4 col-md-4 col-sm-12 wow fadeInLeft border-green" data-wow-duration="2s">
                <img src="img/r-list.png" alt="" class="img-fluid">
                <h2>Rental List</h2>
                <ul>
                    <li>Add Rental Logs</li>
                </ul>
            </div>
        </div>
        <div class="row">
            <div class="col-lg-4 col-md-4 col-sm-12 wow fadeInRight border-green" data-wow-duration="2s">
                <img src="img/f-reports.png" alt="" class="img-fluid">
                <h2>Financial Reports</h2>
                <ul>
                    <li>Generate Financial Statements</li>
                </ul>
            </div>
            <div class="col-lg-4 col-md-4 col-sm-12 wow fadeInRight border-green" data-wow-duration="2s">
                <img src="img/p-jobs.png" alt="" class="img-fluid">
                <h2>Post Job</h2>
                <ul>
                    <li>Post Job</li>
                </ul>
            </div>
        </div>
        </div>
    </div>
    <!-- <span id="team"></span>
    <div class="wel-p5">
        <h1 align="center">Startup Team</h1><br>
        <div class="slideshow-container" align="center">
            <div class="mySlides w3-center w3-animate-opacity">
                <img src="/img/jackie.png" class="img-circle">
                <h4>Jackie Rivas Bustamante</h4>
                <p>Project Manager (Hustler)</p>
            </div>
            <div class="mySlides w3-center w3-animate-opacity">
                <img src="/img/joshua.png" class="img-circle">
                <h4>Joshua Gopio Escarilla</h4>
                <p>Programmer (Hacker)</p>
            </div>
            <div class="mySlides w3-center w3-animate-opacity">
                <img src="/img/lendon.png" class="img-circle">
                <h4>Lendon Ledona Balansag</h4>
                <p>UI Designer (Hipster)</p>
            </div>
            <a class="prev" onclick="plusSlides(-1)">&#10094;</a>
            <a class="next" onclick="plusSlides(1)">&#10095;</a>
        </div>
        <br>
        <div style="text-align:center">
            <span class="dot" onclick="currentSlide(1)"></span>
            <span class="dot" onclick="currentSlide(2)"></span>
            <span class="dot" onclick="currentSlide(3)"></span>
        </div>
    </div> -->
</div>
@endsection

@section('script')
@endsection