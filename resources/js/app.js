// npm install
// npm install --save vue
// npm install --save laravel-echo pusher-js
// npm install --save sweetalert2
// npm install --save-dev vue-plugin-load-script

// require('./bootstrap');

window.Vue = require('vue');
const app = new Vue({
    el: '#app',
});

import swal from 'sweetalert2';
window.swal = swal;

const toast = swal.mixin({
	toast: true,
	position: 'top-end',
	showConfirmButton: false,
	timer: 3000
});

window.toast = toast;