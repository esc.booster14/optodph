<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
 */

## FINAL

Route::get('/', function () {
	return view('welcome');
});

Auth::routes();

Route::group(['middleware' => 'guest'], function () {
	Route::view('/', 'welcome')->name('welcome');

	# Register
	Route::prefix('register')->group(function () {
		Route::view('/driver', 'auth.registers.driver')->name('register.driver');
		Route::view('/operator', 'auth.registers.operator')->name('register.operator');

		Route::post('/driver', 'RegisterController@storeDriver')->name('register.driver');
		Route::post('/operator', 'RegisterController@storeOperator')->name('register.operator');

		Route::get('/verification/{token}', 'RegisterController@verification')->name('register.verification');
		Route::post('/verify', 'RegisterController@verify')->name('register.verify');

		Route::post('/generate_code/{token}', 'RegisterController@generateCode')->name('register.generate_code');

		Route::get('/redirect/{token}/{code}', 'RegisterController@redirect')->name('register.redirect');

		# Subscription
		Route::post('/subscription/select_type/{type}', 'RegisterController@subscriptionType')->name('register.subscription_type');
		Route::view('/subscription/select_bundle', 'auth.registers.subscription')->name('register.subscription_bundle');
		Route::post('/subscription/create', 'RegisterOperatorController@startSubscription')->name('register.subscription');
	});

	Route::get('/gtest', function(){
		$u = \App\User::whereUsername('09068812078')->first();

		return $u;
	});
});

Route::group(['middleware' => 'auth'], function () {
	Route::get('/home', 'HomeController@index')->name('home');
	Route::get('/logout', 'Auth\LoginController@logout')->name('logout');

	# Profile
	Route::prefix('{username}')->group(function () {
		Route::get('/', 'ProfileController@index')->name('profile.index');
		Route::get('/curriculum_vitae', 'ProfileController@cv')->name('profile.cv');
		Route::get('/about', 'ProfileController@about')->name('profile.about');
		Route::get('/photos', 'ProfileController@photos')->name('profile.photos');
		Route::get('/feedbacks', 'ProfileController@feedback')->name('profile.feedback');
	});
		
	Route::prefix('/settings')->group(function () {
		Route::view('/', 'settings.index')->name('settings.index');

		Route::view('/account', 'settings.account')->name('settings.account');
		Route::put('/username/update', 'SettingController@updateUsername')->name('settings.update_username');
		Route::put('/password/update', 'SettingController@updatePassword')->name('settings.update_password');

		Route::view('/user_information', 'settings.information')->name('settings.information');
		Route::put('/user_information/update', 'SettingController@updateInfo')->name('settings.update_info');

		Route::get('/company_info', 'SettingController@companyInformation')->name('settings.company');
		Route::put('/company_info/update', 'SettingController@updateCompany')->name('settings.update_company');
		Route::put('/company_info/contact/update', 'SettingController@updateContact')->name('settings.update_contact');

		Route::put('/social_media', 'SettingController@updateSocial')->name('settings.update_social');

		Route::get('/curriculum_vitae', 'SettingController@cv')->name('settings.cv');
		Route::put('/curriculum_vitae/update', 'SettingController@updateCv')->name('settings.update_cv');

		Route::view('/contact_info', 'settings.contact')->name('settings.contact');
		Route::post('/confirm/email', 'SettingController@confirmEmail')->name('settings.confirm_email');
		Route::post('/confirm/contact_no', 'SettingController@confirmContact')->name('settings.confirm_contact');
		Route::post('/send_code/mail', 'SettingController@sendMail')->name('settings.send_mail');
		Route::post('/send_code/sms', 'SettingController@sendSms')->name('settings.send_sms');

		Route::put('/contact_info/permission/update', 'SettingController@updateContactPermission')->name('permission.update_contact');
		Route::put('/company_info/permission/update', 'SettingController@updateCompanyPermission')->name('permission.update_company');
	});

	Route::resource('jobs', 'JobController');
});

Route::get('/files/{folder}/{filename}', 'FileController@getFile')->where('filename', '^[^/]+$')->name('file');