<?php

use App\User;
use Illuminate\Database\Seeder;

class UserSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        User::create([
        	'name' => 'Optodph',
        	'username' => 'admin',
        	'password' => bcrypt('admin'),
        	'type_id' => 1,
        	'status' => 3,
            'image' => 'dev.png'
        ]);

        User::create([
            'name' => 'Forrest Gump',
            'username' => 'operator',
            'password' => bcrypt('operator'),
            'type_id' => 2,
            'status' => 3,
            'image' => 'user.png'
        ]);

        User::create([
            'name' => 'Bubba Blue',
            'username' => 'driver',
            'password' => bcrypt('driver'),
            'type_id' => 3,
            'status' => 3,
            'image' => 'user.png'
        ]);
    }
}
