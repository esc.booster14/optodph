<?php

use App\Operator;
use Illuminate\Database\Seeder;

class OperatorSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        Operator::create([
        	'user_id' => 2,
        	'type_id' => 1,
        	'name' => 'Forrest Gump',
        	'company' => 'Bubba-Gump Shrimp Co.',
        	'company_logo' => 'company.png',
        	'email' => 'bubbaGump.shrimp@gmail.com',
        	'address' => 'Alaska, Mambaling'
        ]);
    }
}
