<?php

use Illuminate\Database\Seeder;

class DatabaseSeeder extends Seeder
{
    /**
     * Seed the application's database.
     *
     * @return void
     */
    protected $toTruncate = [ 
        'user_types',
		'users',
        'user_contacts',
        'operators'
        // 'sessions'
	];

    public function run()
    {
    	DB::statement('SET FOREIGN_KEY_CHECKS=0;');

    	foreach($this->toTruncate as $table){
    		DB::table($table)->truncate();
    	}

    	DB::statement('SET FOREIGN_KEY_CHECKS=1;');
        
        $this->call([
        	UserTypeSeeder::class,
        	UserSeeder::class,
        	UserContactSeeder::class,
            OperatorSeeder::class,
        ]);

    	// factory(App\Subscriber::class,14)->create();
        // $this->call(UsersTableSeeder::class);
    }
}
