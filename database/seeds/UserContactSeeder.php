<?php

use App\UserContact;
use Illuminate\Database\Seeder;

class UserContactSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        UserContact::create([
        	'user_id' => 1,
        	'email' => 'optodph@gmail.com'
        ]);
    }
}
