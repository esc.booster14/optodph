<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateDriversTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('drivers', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->char('token')->unique(); 
            $table->unsignedBigInteger('user_id');
            $table->char('first_name');
            $table->char('middle_name')->nullable();
            $table->char('family_name');
            $table->char('suffix')->nullable();
            $table->timestamp('birthdate');
            $table->tinyInteger('gender')->comment('(1)Male (2)Female');
            $table->string('work_experience', 5000)->nullable();
            $table->char('school_primary')->nullable();
            $table->char('school_secondary')->nullable();
            $table->char('school_tertiary')->nullable();
            $table->float('height', 7, 2)->nullable()->comment('meter');
            $table->float('weight', 7, 2)->nullable()->comment('kilogram');
            $table->char('religion')->nullable();
            $table->tinyInteger('marital_status')->comment('(1)Single (2)Married (3)Widowed (4)Separated (5)Divorced')->nullable();
            $table->char('nationality')->nullable();
            $table->char('address')->nullable();
            $table->char('barangay')->nullable();
            $table->char('city')->nullable();

            $table->foreign('user_id')
                ->references('id')
                ->on('users')
                ->onDelete('cascade');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('drivers');
    }
}
