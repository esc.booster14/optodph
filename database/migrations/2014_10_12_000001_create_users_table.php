<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateUsersTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('users', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->char('token')->unique();
            $table->char('name');
            $table->char('username');
            $table->char('password');
            $table->unsignedBigInteger('type_id');
            $table->tinyInteger('status')->default(1)->comment('(1)Not validated (2)Validated (3)Authenticated');
            $table->tinyInteger('active_status')->default(0)->comment('(0)False (1)True');
            $table->char('image')->default('user.png')->nullable();
            $table->rememberToken();
            $table->timestamps();

            $table->foreign('type_id')
                ->references('id')
                ->on('user_types');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('users');
    }
}
