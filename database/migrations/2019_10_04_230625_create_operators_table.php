<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateOperatorsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('operators', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->char('token')->unique();
            $table->unsignedBigInteger('user_id');
            $table->tinyInteger('type_id')->comment('(1)Jeepney (2)Taxi (3)Bus');
            $table->char('name')->comment('Operator / Owner name');
            $table->char('company');
            $table->char('company_logo')->default('company.png');
            $table->char('email');
            $table->char('address');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('operators');
    }
}
